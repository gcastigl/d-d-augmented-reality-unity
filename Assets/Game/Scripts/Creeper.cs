﻿using UnityEngine;
using System.Collections;

public class Creeper : MonoBehaviour {

	private static string STATE_ATTACK = "Attack";

	private static string PARAM_ATTACK = "attack";
	private static string PARAM_ATTACK_ID = "attack_id";
	private static string PARAM_ALIVE = "alive";

	private static string TAG_PLAYER = "Player";

	public int strength = 1;

	private Animator animator_;
	private Player player_;
	public Health health_;
	private TurnLock turn_;

	public Health health() {
		return health_;
	}

	public TurnLock turn() {
		return turn_;
	}

	public void restartStatus() {
		health_.restart ();
		animator_.SetBool(PARAM_ALIVE, true);
		animator_.SetBool(PARAM_ATTACK, false);
	}

	void Start () {
		Debug.Log (name + " creeper started!!");
		health_ = GetComponent<Health>();
		if (health_ == null) {
			Debug.LogError("No health component found for this creeper");
		}
		turn_ = gameObject.AddComponent<TurnLock> ();
		player_ = GameObject.FindGameObjectWithTag (TAG_PLAYER).GetComponent<Player>();
		animator_ = GetComponentInChildren<Animator> ();
	}

	void Update () {
		if (!isAlive()) {
			animator_.SetBool(PARAM_ALIVE, false);
			enabled = false;
			player_.health().decrement(-health_.max / 2);
			return;
		}
		AnimatorStateInfo currentBaseState = animator_.GetCurrentAnimatorStateInfo(0);
		if (currentBaseState.IsName(STATE_ATTACK + "_1")) {
			animator_.SetBool(PARAM_ATTACK, false);
			turn ().roundPlayed();
		}
	}

	public bool isAlive() {
		return !health_.isZero();
	}

	public void attack() {
		Debug.Log(name + " | ATTACK > TRUE");
		animator_.SetBool(PARAM_ATTACK, true);
		animator_.SetInteger(PARAM_ATTACK_ID, 1);
		Health health = player_.health();
		health.decrement(strength);
		Debug.Log(name + " attack! | Player HP: " + health.current());
	}

	public void enableOneRound() {
		attack ();
		turn ().enableOneRound ();
	}
}
