﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int max;	

	private int curr_;
	
	void Start () {
		restart ();
	}

	public void restart() {
		curr_ = max;
	}

	public void decrement(int amount) {
		curr_ = Mathf.Min(max, Mathf.Max(0, curr_ - amount));
	}

	public bool isZero() {
		return curr_ == 0;
	}

	public int current() {
		return curr_;
	}
}
