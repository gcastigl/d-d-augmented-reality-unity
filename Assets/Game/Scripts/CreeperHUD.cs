﻿using UnityEngine;
using System.Collections;

public class CreeperHUD : MonoBehaviour {

	public Creeper creeper;
	public TextMesh textMesh;

	void Start () {
		
	}
	
	void Update () {
		Health health = creeper.health();
		textMesh.text = health.current() + " / " + health.max;
	}
}
