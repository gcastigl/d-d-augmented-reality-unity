﻿using UnityEngine;
using System.Collections;

public class TurnLock : MonoBehaviour {

	public bool enabledToPlay;
	public bool played;

	public bool isEnabledToPlay() {
		return enabledToPlay;
	}

	public bool isWaiting() {
		return !isEnabledToPlay();
	}

	public bool isPlayingRound() {
		return enabledToPlay && !played;
	}

	public bool isRoundPlayed() {
		return enabledToPlay && played;
	}

	public void disablePlay() {
		enabledToPlay = false;
	}

	public void enableOneRound() {
		enabledToPlay = true;
		played = false;
	}

	public void roundPlayed() {
		played = true;
	}
}
