﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

public class BattleHandler : MonoBehaviour {

	public Player player;
	public CreeperManager creepers;
	public float minTurnTimeInSec = 0.5f;

	private float turnTimer_ = 0;

	public void onNewBattle() {
		gameObject.SetActive (false);
		player.health().restart();
		player.enableOneRound();
		creepers.onNewBattle();
		Debug.Log (">>>> NEW BATTLE! <<<<<");
	}

	void Update () {
		if (player.activeTarget() != gameObject.transform.parent.gameObject) {
			return;
		}
		turnTimer_ += Time.deltaTime;
		if (turnTimer_ < minTurnTimeInSec) {
			return;
		}
		turnTimer_ = 0;
		TurnLock playerTurn = player.turn();
		TurnLock creepersTurn = creepers.turn();
		if (playerTurn.isRoundPlayed()) {
			playerTurn.disablePlay();
			creepers.enableOneRound();
			Debug.Log("> Creepers' turn!");
		} else if (creepersTurn.isRoundPlayed()) {
			creepersTurn.disablePlay();
			player.enableOneRound();
			Debug.Log("> Player's turn!");
		}
	}

	public bool isFinished() {
		return player.isDead() || creepers.countAliveCreepers() == 0;
	}

}
