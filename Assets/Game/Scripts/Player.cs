﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

public class Player : MonoBehaviour {

	public GUIStyle gui_deadFont;
	public GUIStyle gui_Font;

	private GameObject activeTarget_;
	private BattleHandler currBattle_;

	private Health health_;
	private TurnLock turn_;

	public Camera arcam;
	public int strength = 1;

	public Health health() {
		return health_;
	}

	public TurnLock turn() {
		return turn_;
	}

	public GameObject activeTarget() {
		return activeTarget_;
	}

	void Start () {
		health_ = GetComponent<Health> ();
		if (health_ == null) {
			Debug.LogError("No health component found for this player");
		}
		turn_ = gameObject.AddComponent<TurnLock> ();
	}

	void Update () {
		configureActiveBattle ();
		if (turn_.isPlayingRound()) {
			Creeper creeper = checkIfCreeperHit();
			if (creeper != null) {
				hitCreeper(creeper);
				turn_.roundPlayed();
			}
		}
	}

	private void configureActiveBattle() {
		StateManager sm = TrackerManager.Instance.GetStateManager();
		IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours();
		// TODO: a for here is not necessary. Only check first element
		bool currBattleFinished = currBattle_ != null && currBattle_.isFinished();
		foreach (TrackableBehaviour tb in activeTrackables) {
			if (currBattle_ == null || (currBattleFinished && cameraIsCloseToTarget(tb.gameObject))) {
				setActiveTarget(tb.gameObject);
			} else if (activeTarget_ != null && activeTarget_ != tb.gameObject) {
				BattleHandler battle = tb.gameObject.GetComponentInChildren<BattleHandler>();
				if (battle != null) {
					battle.gameObject.SetActive(false);
				}
			}
		}
	}

	private bool cameraIsCloseToTarget(GameObject imageTarget) {
		if (imageTarget == activeTarget_) {
			return false;
		}
		float distance = Vector3.Distance(imageTarget.transform.position, arcam.transform.position);
		if (distance > 30) {
			return false;
		} else {
			return true;
		}
	}

	private void setActiveTarget(GameObject go) {
		if (activeTarget_ != go && !isDead()) {
			if (currBattle_ != null) {
				health().max += 5;
			}
			activeTarget_ = go;
			GameObject room = activeTarget_.transform.GetChild(0).gameObject;
			room.SetActive(true);
			currBattle_ = activeTarget_.GetComponentInChildren<BattleHandler>();
			if (currBattle_ == null) {
				Debug.LogError("No Battlehandler found on target: " + activeTarget_.name);
			}
			currBattle_.onNewBattle();
			currBattle_.gameObject.SetActive(true);
			Debug.Log(">>>> Active target: " + activeTarget_);
		}
	}

	private Creeper checkIfCreeperHit() {
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				Creeper creeper = hit.collider.gameObject.GetComponent<Creeper>();
				if (creeper != null && creeper.isAlive()) {
					return creeper;
				}
			}
		}
		return null;
	}

	private void hitCreeper(Creeper creeper) {
		Health health = creeper.health();
		health.decrement(strength * Random.Range(1, 3));
		Debug.Log("You hit: " + creeper.name + " | Heath: " + health.current());
		if (health.isZero()) {
			Debug.Log(creeper.name + " is dead!");
		}
	}

	public void enableOneRound() {
		if (!isDead()) {
			turn().enableOneRound();
		} else {
			turn().roundPlayed();
		}
	}

	public bool isDead() {
		return health_.isZero();
	}
	
	void OnGUI() {
		if (isDead ()) {
			deadMessageGUI ();
		} else {
			currentStatsGUI();
		}
	}

	private void currentStatsGUI() {
		float h = Screen.height;
		float w = Screen.width;
		gui_Font.fontSize = (int)(h * 0.08);
		GUI.TextField(new Rect(10, 10, w * 0.3f, h * 0.1f), "HP: " + health_.current() + " / " + health_.max, gui_Font);
		if (activeTarget_ != null) {
			string turnName = turn().isPlayingRound() ? "Player" : "Enemy";
			float xOffset = 10;
			GUI.TextField(new Rect(xOffset, h * 0.9f - 10, w - 2 * xOffset, h * 0.1f), "Turn: " + turnName + " | " + activeTarget_.name, gui_Font);
		}
	}

	private void deadMessageGUI() {
		gui_deadFont.fontSize = (int) (Screen.width * 0.10f);
		float w = Screen.width * 0.75f;
		float h = 50;
		float x = Screen.width/2 - (w / 2);
		float y = Screen.height/2 - (h / 2);
		string message = "You shall not pass!!";
		GUI.Label(new Rect (x, y, w, h), message, gui_deadFont);
	}
}
