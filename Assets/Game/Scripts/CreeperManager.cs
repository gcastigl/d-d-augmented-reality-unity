﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreeperManager : MonoBehaviour {

	public Player player;

	public GameObject[] prefabsGOs;
	public GameObject[] positionsGos;

	private TurnLock turn_;
	private GameObject[] creepersGOs_;
	private int currCreeperIndex_;

	public TurnLock turn() {
		return turn_;
	}

	public void onNewBattle() {
		List<GameObject> creepersList = new List<GameObject>();
		for (int i = 0; i < positionsGos.Length; i++) {
			GameObject positionGO = positionsGos[i];
			if (positionGO.transform.childCount == 1) {
				Destroy(positionGO.transform.GetChild(0).gameObject);
			}
			if (creepersList.Count == 0 || Random.value < 0.75) {
				int creeperIndx = Random.Range(0, prefabsGOs.Length);
				GameObject creeper = GameObject.Instantiate(prefabsGOs[creeperIndx]) as GameObject;
				creeper.transform.parent = positionGO.transform;
				creeper.transform.localPosition = Vector3.zero;
				creeper.transform.localScale = prefabsGOs[creeperIndx].transform.localScale;
				creepersList.Add(creeper);
			}
		}
		creepersGOs_ = creepersList.ToArray();
	}

	void Start () {
		turn_ = gameObject.AddComponent<TurnLock> ();
	}
	
	void Update () {
		if (turn_.isPlayingRound ()) {
			if (!hasCurrentCreeper()) {
				turn_.roundPlayed();
			} else {
				playCurrentCreeper();
			}
		}
	}

	private void playCurrentCreeper() {
		Creeper creeper = getCurrCreeper();
		if (!creeper.isAlive()) {
			creeper.turn().roundPlayed();
		}
		if (creeper.turn().isRoundPlayed()) {
			findNextAliveCreeper();
			if (hasCurrentCreeper()) {
				getCurrCreeper().enableOneRound();
			}
		}
	}

	private Creeper getCurrCreeper() {
		return creepersGOs_ [currCreeperIndex_].GetComponent<Creeper> ();
	}

	private void findNextAliveCreeper() {
		do {
			currCreeperIndex_++;
		} while(hasCurrentCreeper() && !getCurrCreeper().isAlive());
	}

	private bool hasCurrentCreeper() {
		return currCreeperIndex_ < creepersGOs_.Length;
	}

	public void enableOneRound() {
		if (countAliveCreepers () > 0) {
			turn ().enableOneRound();
			currCreeperIndex_ = -1;
			findNextAliveCreeper ();
			if (hasCurrentCreeper ()) {
				getCurrCreeper ().enableOneRound ();
			}
		} else {
			turn().roundPlayed();
		}
	}

	public int countAliveCreepers() {
		int count = 0;
		foreach (GameObject creepGo in creepersGOs_) {
			Creeper creeper = creepGo.GetComponent<Creeper>();
			count += creeper.isAlive() ? 1 : 0;
		}
		return count;
	}
}
